module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/pajamas-playground" : "/",
  configureWebpack: {
    resolve: {
      alias: {
        vue$: "vue/dist/vue.esm.js"
      }
    }
  }
};
